# Support for generating code coverage reports with gcovr.
# Options:
# GENERATE_COVERAGE - If the code should be built with coverage metrics enabled. Default yes.
# Variables:

option(GENERATE_COVERAGE "Build with coverage metrics" ON)

if(GENERATE_COVERAGE AND CMAKE_BUILD_TYPE STREQUAL "Debug" AND NOT MSVC AND NOT CMAKE_CROSSCOMPILING)
    find_package(gcovr)
    if(GCOVR_FOUND)
        add_definitions("--coverage")
        set(CMAKE_SHARED_LINKER_FLAGS  "${CMAKE_SHARED_LINKER_FLAGS} --coverage" )
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")

        # if we've built with Clang, we need to use llvm-cov to process files, otherwise just use gcov
        if(CMAKE_C_COMPILER_ID STREQUAL "Clang")
            set(GCOV_ENV "llvm-cov gcov")
        else()
            set(GCOV_ENV "gcov")
        endif()

        add_custom_target(coverage
                          COMMAND ${CMAKE_COMMAND} -E make_directory "${CMAKE_BINARY_DIR}/coverage_reports"
                          COMMAND ${CMAKE_COMMAND} -E env GCOV=${GCOV_ENV} ${GCOVR_EXECUTABLE}
                                  -r "${CMAKE_SOURCE_DIR}" --object-directory "${CMAKE_BINARY_DIR}"
                                  --exclude ".*_deps\/.*" --exclude ".*tests\/.*"
                                  --print-summary --html-details
                                  -o "${CMAKE_BINARY_DIR}/coverage_reports/coverage_report_detailed.html"
                                  --delete
                                  ${CMAKE_BINARY_DIR}
                          WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                          COMMENT "Generating coverage data using gcovr"
                          VERBATIM)
    else()
        message(STATUS "Failed to find gcovr module. Coverage report not generated")
    endif()
endif()
