# Add an option to run clang-tidy and and option to allow clang-tidy to
# automatically fix any issues.
#
# Please note that clang_tidy() must be called for the targets to check/fix.
#
# Example:
#
#   add_library(foo ${FOO_HEADERS} ${FOO_SOURCES})
#   clang_tidy(foo)
#

include(CMakeDependentOption)

# Options for clang-tidy
option(STYLE_CHECK "Check the coding style of C++ code" OFF)
cmake_dependent_option(STYLE_FIX "Attempt to automatically fix coding style issues" OFF
                       "STYLE_CHECK" OFF)
cmake_dependent_option(STYLE_WERROR "Treat style warnings as errors" OFF
                       "STYLE_CHECK" OFF)

# function to enable clang-tidy for the given target.
function(clang_tidy _target)
    if(NOT TARGET ${_target})
        message(FATAL_ERROR "clang_tidy can only be used on targets (got ${_target}.)")
    endif()

    # default command list for clang-tidy
    set(_clang_tidy_cmd_list "clang-tidy")

    # handle automatic fixing
    if(STYLE_FIX)
        list(APPEND _clang_tidy_cmd_list "-fix")
    endif()

    # handle warnings-as-errors
    if(STYLE_WERROR)
        list(APPEND _clang_tidy_cmd_list "-warnings-as-errors=*")
    endif()

    if(STYLE_CHECK)
        set_target_properties(${_target} PROPERTIES CXX_CLANG_TIDY "${_clang_tidy_cmd_list}")
    endif()
endfunction()
