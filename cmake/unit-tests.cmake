include(GoogleTest)

# add a test with dependencies on gtest and add it to CTest.
# add_gtest(<name> [INCLUDES ...] [LIBRARIES ...] SOURCES source1 [source2 ...])
# where INCLUDES signal one or more include directories/files, LIBRARIES signal 
# one or more libraries to link to and SOURCES indicate  source files. 
# One or more source files must be added.
function(add_gtest name)
    set(options)
    set(onevalueargs)
    set(multivalueargs LIBRARIES INCLUDES SOURCES)
    cmake_parse_arguments(ADD_GTEST "${options}" "${onevalueargs}" "${multivalueargs}" ${ARGN})
    set(test_name "test_${name}")

    add_executable(${test_name} ${ADD_GTEST_SOURCES})
    target_include_directories(${test_name} BEFORE PRIVATE ${ADD_GTEST_INCLUDES})
    target_link_libraries(${test_name} gtest_main gtest gmock ${ADD_GTEST_LIBRARIES})
    target_compile_options(${test_name} PRIVATE $<$<CXX_COMPILER_ID:GNU>:-Wall>)

    gtest_discover_tests(${test_name} TEST_PREFIX "${name}:"
                         EXTRA_ARGS "--gtest_output=xml:${CMAKE_BINARY_DIR}/test_reports/")
endfunction()

